#!/bin/bash
# This script configures the system
git clone --quiet https://github.com/lazyfox81/dotfiles.git
git clone --quiet https://github.com/lazyfox81/manuals.git
git clone --quiet https://github.com/lazyfox81/bin.git
git clone --quiet https://github.com/lazyfox81/python.git
if [ -d backup_dot ]
then
    echo "Backup folder exist"
else
    mkdir backup_dot
fi
list=(".xinitrc" ".zshrc" ".zprofile" ".vimperatorrc" ".vimrc")
echo ${list[*]}
for file in ${list[*]}
do
    if [ -f $file ] 
    then
        mv $file $HOME/backup_dot
    fi
done
ln -s $HOME/dotfiles/.xinitrc .xinitrc
ln -s $HOME/dotfiles/.zshrc .zshrc
ln -s $HOME/dotfiles/.zprofile .zprofile
ln -s $HOME/dotfiles/.vimrc .vimrc
ln -s $HOME/dotfiles/.vimperatorrc .vimperatorrc
