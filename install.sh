#!/bin/bash
# Archlinux installing script
# Before installing to partition a disk with 'fdisk' utility
# !!! Script not work now !!!
# Install Arch by type command manually

# fdisk /dev/sda
# Device     Boot  Size     Type
# /dev/sda1  *     50G      HPFS/NTFS/exFAT
# /dev/sda2        200M     Linux
# /dev/sda3        8G       Linux swap / Solaris
# /dev/sda4        873.3G   Extended
# /dev/sda5        30G      Linux
# /dev/sda6        848.3G   Linux

mkfs.ext2 -L boot /dev/sda2
mkfs.ext4 -L root /dev/sda5
mkfs.ext4 -L home /dev/sda6

mkswap /dev/sda3
swapon /dev/sda3

mkdir /mnt/arch
mount /dev/sda5 /mnt/arch

mkdir /mnt/arch/{boot,home}
mount /dev/sda2 /mnt/arch/boot
mount /dev/sda6 /mnt/arch/home

# Install the base system
pacstrap /mnt/arch base base-devel

# Generating /etc/fstab
genfstab -p -L /mnt/arch >> /mnt/arch/etc/fstab

# Mount the temporary api filesystems
cd /mnt/arch
mount -t proc proc proc/
mount --rbind /sys sys/
mount --make-rslave sys/
mount --rbind /dev dev/
mount --make-rslave dev/

# To use an internet connection in the chroot environment copy over the DNS
# details:
cp /etc/resolv.conf etc/resolv.conf

# Change root
chroot /mnt/arch /bin/bash
source /etc/profile
export PS1="(chroot) $PS1"

# Get mirrorlist for new system
nano -w /etc/pacman.d/mirrorlist

# Set the host name
echo comp > /etc/hostname
# Set the timezone
ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime

# Generate locales
echo "en_US.UTF-8" > /etc/locale.gen
echo "en_US.ISO-8859-1" >> /etc/locale.gen
locale-gen

echo "LANG=en_US.utf8" > /etc/locale.conf

# Create a new initial RAM disk with
mkinitcpio -p linux

# Enabling repositories
nano -w /etc/pacman.conf
pacman -Syu

# GRUB
pacman -S grub ntfs-3g os-prober
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
# GRUB must found vmlinuz, initramfs, Windows

# Install apllication
pacman -S   zsh rxvt-unicode urxvt-perl xclip \
            alsa-utils alsa-lib acpi \
            xorg-server xorg-server-utils xorg-xinit xorg-utils \
            xf86-video-intel nvidia nvidia-utils nvidia-libgl mesa \
            lib32-nvidia-utils lib32-nvidia-libgl lib32-mesa \
            i3-wm firefox pcmanfm git \
            gtk-engine-murrine feh lxappearance \
            gnome-themes-standard \
            ttf-droid ttf-liberation ttf-dejavu \
            unzip unrar p7zip

# Install 'yaourt'
git clone https://aur.archlinux.org/package-query.git
git clone https://aur.archlinux.org/yaourt.git
cd package-query
makepkg -si
cd ../yaourt
makepkg -si
cd ..
rm -rf package-query/ yaourt/

yaourt -S   ntfs-3g-fuse ttf-font-awesome powerline-fonts-git \
            dmenu-xft-mouse-height-fuzzy-history j4-dmenu-desktop \
            gtk-theme-arc-git nitrux-icon-theme

# Enable bumblebee
systemctl enable bumblebeed.service

# Create user
useradd -m -g users -G audio,wheel,bumblebee,disk -s /bin/zsh lazyfox

# Internet connection
enable dhcpcd.service
passwd
passwd lazyfox

# Umount and reboot
exit
cd /
umount --recursive /mnt/arch/
reboot
